package ui;

import apptemplate.AppTemplate;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Created by David on 11/18/2016.
 */
public class ProfileDialogSingleton extends Stage {

    AppTemplate appTemplate;

    private static ProfileDialogSingleton singleton = null;

    private Label messageLabel;

    Text username;

    private ProfileDialogSingleton() { }

    /**
     * A static accessor method for getting the singleton object.
     *
     * @return The one singleton dialog of this object type.
     */
    public static ProfileDialogSingleton getSingleton() {
        if (singleton == null)
            singleton = new ProfileDialogSingleton();
        return singleton;
    }

    public void setMessageLabel(String messageLabelText) {
        messageLabel.setText(messageLabelText);
    }

    /**
     * This function fully initializes the singleton dialog for use.
     *
     * @param owner The window above which this dialog will be centered.
     */
    public void init(Stage owner, AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        initModality(Modality.WINDOW_MODAL); // modal => messages are blocked from reaching other windows
        initOwner(owner);

        messageLabel = new Label();

        Label usernameLabel = new Label("Username:");
        username = new Text();
        HBox usernameBox = new HBox(10);
        usernameBox.getChildren().addAll(usernameLabel,username);

        Button logoutButton = new Button("Logout");
        logoutButton.setOnMouseClicked(event -> {
            appTemplate.getWorkspaceComponent().logout();
            this.close();
        });
        Button closeButton = new Button("Close");
        closeButton.setOnMouseClicked(event -> {
            this.close();
        });
        HBox buttonPane = new HBox(10);
        buttonPane.getChildren().addAll(logoutButton,closeButton);

        VBox messagePane = new VBox();
        messagePane.setAlignment(Pos.CENTER);
        messagePane.getChildren().add(messageLabel);
        messagePane.getChildren().add(usernameBox);
        messagePane.getChildren().add(buttonPane);

        messagePane.setPadding(new Insets(80, 60, 80, 60));
        messagePane.setSpacing(20);
        Scene messageScene = new Scene(messagePane);
        this.setScene(messageScene);
    }

    /**
     * This method loads a custom message into the label and
     * then pops open the dialog.
     *
     * @param title   The title to appear in the dialog window.
     * @param message Message to appear inside the dialog.
     */
    public void show(String title, String message) {
        setTitle(title); // set the dialog title
        setMessageLabel(message); // message displayed to the user
        showAndWait(); // opens the dialog, and waits for the user to resolve using one of the given choices
    }

    public void updateUsername(String usernameString){
        username.setText(usernameString);
    }
}
