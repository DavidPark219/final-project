package ui;

import apptemplate.AppTemplate;
import components.AppDataComponent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

//import static settings.InitializationParameters.ERROR_DIALOG_BUTTON_LABEL;

/**
 * This class serves to present custom text messages to the user when
 * events occur. Note that it always provides the same controls, a label
 * with a message, and a single ok button.
 *
 * @author Richard McKenna, Ritwik Banerjee
 * @author ?
 * @version 1.0
 */
public class CreateProfileDialogSingleton extends Stage {

    AppTemplate appTemplate;

    private static CreateProfileDialogSingleton singleton = null;

    private Label messageLabel;

    String usernameString;
    String passwordString;

    private CreateProfileDialogSingleton() { }
    
    /**
     * A static accessor method for getting the singleton object.
     *
     * @return The one singleton dialog of this object type.
     */
    public static CreateProfileDialogSingleton getSingleton() {
        if (singleton == null)
            singleton = new CreateProfileDialogSingleton();
        return singleton;
    }

    public void setMessageLabel(String messageLabelText) {
        messageLabel.setText(messageLabelText);
    }

    /**
     * This function fully initializes the singleton dialog for use.
     *
     * @param owner The window above which this dialog will be centered.
     */
    public void init(Stage owner, AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        initModality(Modality.WINDOW_MODAL); // modal => messages are blocked from reaching other windows
        initOwner(owner);

        messageLabel = new Label();

        TextField username = new TextField();
        PasswordField password = new PasswordField();

        username.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                if (password.getText() != null) {
                    usernameString = username.getText();
                    passwordString = password.getText();
                    appTemplate.getDataComponent().createProfile(usernameString, passwordString);
                    this.close();
                }
            }
        });

        password.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                if (password.getText() != null) {
                    usernameString = username.getText();
                    passwordString = password.getText();
                    appTemplate.getDataComponent().createProfile(usernameString, passwordString);
                    this.close();
                }
            }
        });

        Button createProfileButton = new Button("Create");
        createProfileButton.setOnMouseClicked(event ->{
            usernameString = username.getText();
            passwordString = password.getText();
            appTemplate.getDataComponent().createProfile(usernameString,passwordString);
            this.close();
        });
        Button cancelButton = new Button("Cancel");
        cancelButton.setOnMouseClicked(event -> {
            this.close();
        });

        Label usernameLabel = new Label("Username:");
        HBox usernameBox = new HBox(10);
        usernameBox.getChildren().addAll(usernameLabel,username);
        Label passwordLabel = new Label("Password:");
        HBox passwordBox = new HBox(10);
        passwordBox.getChildren().addAll(passwordLabel,password);

        VBox messagePane = new VBox();
        HBox buttonPane = new HBox(10);
        buttonPane.getChildren().addAll(createProfileButton,cancelButton);
        buttonPane.setAlignment(Pos.CENTER);
        messagePane.setAlignment(Pos.CENTER);
        messagePane.getChildren().add(messageLabel);
        messagePane.getChildren().addAll(usernameBox,passwordBox);
        messagePane.getChildren().add(buttonPane);

        messagePane.setPadding(new Insets(80, 60, 80, 60));
        messagePane.setSpacing(20);

        Scene messageScene = new Scene(messagePane);
        this.setScene(messageScene);
    }

    /**
     * This method loads a custom message into the label and
     * then pops open the dialog.
     *
     * @param title   The title to appear in the dialog window.
     * @param message Message to appear inside the dialog.
     */
    public void show(String title, String message) {
        setTitle(title); // set the dialog title
        setMessageLabel(message);
        showAndWait(); // opens the dialog, and waits for the user to resolve using one of the given choices
    }

    public String getUsernameString(){
        return usernameString;
    }

    public String getPasswordString() {
        return passwordString;
    }
}