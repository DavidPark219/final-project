package data;

import apptemplate.AppTemplate;
import components.AppDataComponent;
import gui.Workspace;
import ui.ProfileDialogSingleton;

import java.io.*;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by David on 11/6/2016.
 */
public class GameData implements AppDataComponent{

    final File profileResource = new File("BuzzWord/resources/profile/profiles.txt");
    private String username;
    private String password;
    private String encrypted;
    private byte[] digest;

    int levelsCompleted;

    boolean validAccount;

    private MessageDigest messageDigest;

    public AppTemplate appTemplate;

    public GameData(AppTemplate appTemplate) throws IOException {
        this.appTemplate = appTemplate;
        if (profileResource == null){
            profileResource.mkdir();
        }
        levelsCompleted = 0;
        validAccount = false;
    }

    public void createProfile(String username, String password) {
        boolean accountExists = false;
        levelsCompleted = 0;

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(profileResource))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                //Make sure account doesn't exist
                if (line.substring(0,line.indexOf("|")).equals(username)) {
                    accountExists = true;
                    break;
                }
            }
            if (!accountExists) {
                try {
                    this.username = username;
                    this.password = password;
                    updateUsernameDialog();

                    messageDigest = MessageDigest.getInstance("MD5");
                    digest = messageDigest.digest(password.getBytes());
                    encrypted = String.format("%032X", new BigInteger(1, digest));
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }

                try {
                    FileWriter fileWriter = new FileWriter(profileResource, true);
                    BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                    bufferedWriter.write(username + "|" + encrypted + "/" + levelsCompleted);
                    bufferedWriter.newLine();
                    bufferedWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
                workspace.layoutPreGameGUI();
                validAccount = true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void login(String username, String password){
        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(profileResource))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String passwordLine;
                if (line.contains(username)) {
                    passwordLine = line.substring(line.indexOf("|") + 1,line.indexOf("/"));
                    try {
                        messageDigest = MessageDigest.getInstance("MD5");
                        digest = messageDigest.digest(password.getBytes());
                        encrypted = String.format("%032X", new BigInteger(1, digest));
                        if (encrypted.equals(passwordLine)) {
                            this.username = username;
                            this.password = password;
                            levelsCompleted = Integer.parseInt(line.substring(line.indexOf("/") + 1));
                            workspace.updateLevelSelectionGrid(levelsCompleted);
                            updateUsernameDialog();

                            workspace.layoutPreGameGUI();
                            validAccount = true;
                        }
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void updateUsernameDialog() {
        ProfileDialogSingleton profileDialog = ProfileDialogSingleton.getSingleton();
        profileDialog.updateUsername(username);
    }

    @Override
    public void reset() {
        validAccount = false;
        this.username = null;
        this.password = null;
    }

    public boolean getValidAccount() {
        return validAccount;
    }

    public void logout(){
        reset();
    }

    public void addLevelCompleted(){
        levelsCompleted++;
        save();
    }

    public void save() {
        String oldFileName = "BuzzWord/resources/profile/profiles.txt";
        String tmpFileName = "BuzzWord/resources/profile/profilesTemp.txt";

        BufferedReader br = null;
        BufferedWriter bw = null;
        try {
            br = new BufferedReader(new FileReader(oldFileName));
            bw = new BufferedWriter(new FileWriter(tmpFileName));
            String line;
            while ((line = br.readLine()) != null) {
                if (line.contains(username))
                    line = username + "|" + encrypted + "/" + levelsCompleted;
                bw.write(line);
                bw.newLine();
            }
        } catch (Exception e) {
            return;
        } finally {
            try {
                if(br != null)
                    br.close();
            } catch (IOException e) {
            }
            try {
                if(bw != null)
                    bw.close();
            } catch (IOException e) {
            }
        }
        File oldFile = new File(oldFileName);
        oldFile.delete();
        File newFile = new File(tmpFileName);
        newFile.renameTo(oldFile);
    }
    public int getLevelsCompleted() {
        return levelsCompleted;
    }
}
