package buzzWord;

/**
 * Created by David on 11/6/2016.
 */
public enum BuzzWordProperties {
    LEFT_PANE,
    RIGHT_PANE,
    BUTTON,
    HEADING_LABEL,
    CIRCLE,
    GAME_MODE_LABEL,
    CIRCLE_SELECTED,
    GAME_LABEL,
    TABLE;
}
