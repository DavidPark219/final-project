package gui;

import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;

/**
 * Created by David on 11/11/2016.
 */
public class LetterCircle extends StackPane {
    public static final String color = "#4D69B6";

    boolean visited;

    boolean used;

    Circle circle;
    public LetterCircle() {
        super();
        addCircle();
        this.visited = false;
        this.used = false;
    }

    private void addCircle() {
        circle = new Circle(50);
        circle.setFill(Paint.valueOf(color));
        this.getChildren().add(circle);
    }

    public void addLetter(String letter) {
        Text letterText = new Text(letter);
        letterText.setFont(Font.font("Helvetica", 25));
        letterText.setFill(Color.WHITE);
        letterText.setBoundsType(TextBoundsType.VISUAL);
        this.getChildren().add(letterText);
    }

    public Circle getCircle() {
        return circle;
    }

    public Text getText() {
        return (Text) this.getChildren().get(1);
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }
}
