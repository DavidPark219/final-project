package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.BuzzWordController;
import data.GameData;
import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.util.Duration;
import propertymanager.PropertyManager;
import ui.*;

import java.io.IOException;

import static buzzWord.BuzzWordProperties.*;

/**
 * Created by David on 11/6/2016.
 */
public class Workspace extends AppWorkspaceComponent {

    AppTemplate app;
    AppGUI      gui;
    BuzzWordController controller;

    Pane leftBackground;
    Button createProfileButton;
    Button loginButton;
    Button profileButton;
    ComboBox gameModeSelection;
    String selection;
    Button startButton;
    Button closeButton;
    Button homeButton;

    Pane rightBackground;
    Label headingLabel;
    LetterCircleGrid menuGrid;
    Label gameModeLabel;
    LetterCircleGrid levelSelectionGrid;
    int level;
    LetterCircleGrid gameGrid;
    GridConnector gridConnector;
    StackPane gameGridPane;
    Label timeRemainingLabel;
    Label wordProgress;
    String word;
    BorderPane pointTable;
    TableView<wordPoint> gameProgress;
    Label totalPoints;
    Label target;
    Label levelLabel;
    Button playPauseButton;

    ObservableList<wordPoint> sampleData;
    int totalScore;
    int targetScore;
    int timeLeft;
    Timeline timeline;

    GameData gameData;

    Button replayButton;
    Button nextButton;

    int selectedColumn;
    int selectedRow;

    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();
        controller = (BuzzWordController) gui.getFileController();
        initialize();
        layoutLoginGUI();
    }

    private void initialize() {
        word = "";
        leftBackground = new Pane();
        rightBackground = new Pane();

        gameData = (GameData) app.getDataComponent();

        createProfileButton = new Button("Create New Profile");
        createProfileButton.layoutXProperty().bind(leftBackground.widthProperty().divide(6));
        createProfileButton.layoutYProperty().bind(leftBackground.heightProperty().divide(8));
        createProfileButton.setOnMouseClicked(event -> {
            CreateProfileDialogSingleton createProfileDialog = CreateProfileDialogSingleton.getSingleton();
            createProfileDialog.show("Create Profile", "Create Profile:");
        });
        loginButton = new Button("Login");
        loginButton.prefWidthProperty().bind(createProfileButton.widthProperty());
        loginButton.layoutXProperty().bind(leftBackground.widthProperty().divide(6));
        loginButton.layoutYProperty().bind(leftBackground.heightProperty().divide(4));
        loginButton.setOnMouseClicked(event -> {
            LoginDialogSingleton loginDialog = LoginDialogSingleton.getSingleton();
            loginDialog.show("Login","Login:");
        });

        headingLabel = new Label("BuzzWord");
        headingLabel.layoutXProperty().bind(rightBackground.widthProperty().divide(2.8));
        headingLabel.layoutYProperty().bind(rightBackground.heightProperty().divide(50));
        menuGrid = new LetterCircleGrid(app);
        menuGrid.buzzWordGrid();
        menuGrid.layoutXProperty().bind(rightBackground.widthProperty().divide(4.2));
        menuGrid.layoutYProperty().bind(rightBackground.heightProperty().divide(4));
        closeButton = new Button("X");
        closeButton.layoutXProperty().bind(rightBackground.widthProperty().divide(1.1));
        closeButton.layoutYProperty().bind(rightBackground.heightProperty().divide(50));
        closeButton.setOnMouseClicked(event -> {
            gameGridPane.setVisible(false);
            YesNoCancelDialogSingleton confirmDialog = YesNoCancelDialogSingleton.getSingleton();
            confirmDialog.show("Exit?", "Do you wish to exit?");
            timeline.pause();
            if (confirmDialog.getSelection().equals(YesNoCancelDialogSingleton.YES))
                Platform.exit();
            else {
                gameGridPane.setVisible(true);
                timeline.play();
            }
        });

        workspace = new HBox();
        workspace.getChildren().addAll(leftBackground, rightBackground);

        leftBackground.prefHeightProperty().bind(workspace.heightProperty());
        leftBackground.prefWidthProperty().bind(workspace.widthProperty().divide(2.5));
        rightBackground.prefHeightProperty().bind(workspace.heightProperty());
        rightBackground.prefWidthProperty().bind(workspace.widthProperty());

        activateWorkspace(gui.getAppPane());

        //Init the other GUI elements
        profileButton = new Button("Profile");
        profileButton.prefWidthProperty().bind(createProfileButton.widthProperty());
        profileButton.layoutXProperty().bind(leftBackground.widthProperty().divide(6));
        profileButton.layoutYProperty().bind(leftBackground.heightProperty().divide(8));
        profileButton.setOnMouseClicked(event -> {
            ProfileDialogSingleton profileDialog = ProfileDialogSingleton.getSingleton();
            profileDialog.show("Profile", "Your Profile:");
        });
        gameModeSelection = new ComboBox();
        gameModeSelection.getItems().addAll("English Dictionary","Places","Food");
        gameModeSelection.setValue("Pick a game mode");
        gameModeSelection.setOnAction(event -> {
            selection = (String) gameModeSelection.getSelectionModel().getSelectedItem();
            gameGrid.updateSelection(selection);
            startButton.setDisable(false);
        });
        gameModeSelection.prefWidthProperty().bind(createProfileButton.widthProperty());
        gameModeSelection.prefHeightProperty().bind(createProfileButton.heightProperty());
        gameModeSelection.layoutXProperty().bind(leftBackground.widthProperty().divide(6));
        gameModeSelection.layoutYProperty().bind(leftBackground.heightProperty().divide(4));
        startButton = new Button("Start Playing");
        startButton.setDisable(true);
        startButton.prefWidthProperty().bind(createProfileButton.widthProperty());
        startButton.layoutXProperty().bind(leftBackground.widthProperty().divide(6));
        startButton.layoutYProperty().bind(leftBackground.heightProperty().divide(2.5));
        startButton.setOnMouseClicked(event -> {
            layoutLevelSelectionGUI();
        });
        homeButton = new Button("Home");
        homeButton.prefWidthProperty().bind(createProfileButton.widthProperty());
        homeButton.layoutXProperty().bind(leftBackground.widthProperty().divide(6));
        homeButton.layoutYProperty().bind(leftBackground.heightProperty().divide(4));
        homeButton.setOnMouseClicked(event -> layoutPreGameGUI());
        gameModeLabel = new Label();
        gameModeLabel.layoutXProperty().bind(rightBackground.widthProperty().divide(3));
        gameModeLabel.layoutYProperty().bind(rightBackground.heightProperty().divide(5.9));
        levelSelectionGrid = new LetterCircleGrid(app);
        levelSelectionGrid.levelSelectionGrid(gameData.getLevelsCompleted());
        levelSelectionGrid.layoutXProperty().bind(rightBackground.widthProperty().divide(4.5));
        levelSelectionGrid.layoutYProperty().bind(rightBackground.heightProperty().divide(4));
        for (Node c : levelSelectionGrid.getChildren()) {
            LetterCircle circle = (LetterCircle) c;
            int column = GridPane.getColumnIndex(c);
            int row = GridPane.getRowIndex(c);
            c.setOnMouseClicked(event -> {
                layoutGameGUI(column, row);
                targetScore = gameGrid.getTargetScore();
                target.setText("Target: \n" + targetScore + " Points");
                selectedColumn = column;
                selectedRow = row;
            });
        }
        gameGrid = new LetterCircleGrid(app);
        gameGrid.gameGrid();
        gridConnector = new GridConnector(app);
        gameGridPane = new StackPane();
        gameGridPane.setMargin(gridConnector, new Insets(0 ,0,0,60));
        gameGridPane.getChildren().addAll(gridConnector,gameGrid);
        gameGridPane.layoutXProperty().bind(rightBackground.widthProperty().divide(4.5));
        gameGridPane.layoutYProperty().bind(rightBackground.heightProperty().divide(10));


        timeRemainingLabel = new Label("Time Remaining:\n" + timeLeft + " Seconds");
        timeRemainingLabel.layoutXProperty().bind(rightBackground.widthProperty().divide(1.3));
        timeRemainingLabel.layoutYProperty().bind(rightBackground.heightProperty().divide(6));
        wordProgress = new Label();
        wordProgress.layoutXProperty().bind(rightBackground.widthProperty().divide(1.3));
        wordProgress.layoutYProperty().bind(rightBackground.heightProperty().divide(3));
        pointTable = new BorderPane();
        gameProgress = new TableView<>();
        sampleData = FXCollections.observableArrayList();
        TableColumn words = new TableColumn("Words Guessed");
        words.setMinWidth(100);
        words.setCellValueFactory(new PropertyValueFactory<>("word"));
        TableColumn points = new TableColumn("Points Earned");
        points.setMinWidth(100);
        points.setCellValueFactory(new PropertyValueFactory<>("points"));
        gameProgress.setItems(sampleData);
        gameProgress.getColumns().addAll(words,points);
        gameProgress.setMaxHeight(200);
        totalPoints = new Label("Total Points:");
        pointTable.setTop(gameProgress);
        pointTable.setBottom(totalPoints);
        pointTable.layoutXProperty().bind(rightBackground.widthProperty().divide(1.3));
        pointTable.layoutYProperty().bind(rightBackground.heightProperty().divide(2.3));
        target = new Label("Target:");
        target.layoutXProperty().bind(rightBackground.widthProperty().divide(1.3));
        target.layoutYProperty().bind(rightBackground.heightProperty().divide(1.14));
        levelLabel = new Label();
        levelLabel.layoutXProperty().bind(rightBackground.widthProperty().divide(2.5));
        levelLabel.layoutYProperty().bind(rightBackground.heightProperty().divide(1.289));
        playPauseButton = new Button("Pause");
        playPauseButton.layoutXProperty().bind(rightBackground.widthProperty().divide(2.5));
        playPauseButton.layoutYProperty().bind(rightBackground.heightProperty().divide(1.15));
        playPauseButton.setOnMouseClicked(event -> {
            if (playPauseButton.getText().equals("Pause")) {
                playPauseButton.setText("Play");
                gameGridPane.setVisible(false);
                timeline.pause();
            }
            else {
                playPauseButton.setText("Pause");
                gameGridPane.setVisible(true);
                timeline.play();
            }
        });
        replayButton = new Button("Replay Level");
        replayButton.layoutXProperty().bind(rightBackground.widthProperty().divide(7));
        replayButton.layoutYProperty().bind(rightBackground.heightProperty().divide(1.15));
        replayButton.setOnMouseClicked(e -> {
            layoutGameGUI(level);
        });
        nextButton = new Button("Next Level");
        nextButton.layoutXProperty().bind(rightBackground.widthProperty().divide(1.75));
        nextButton.layoutYProperty().bind(rightBackground.heightProperty().divide(1.15));
        nextButton.setDisable(true);
        nextButton.setOnMouseClicked(e -> {
            layoutGameGUI(level + 1);
        });
    }

    public void layoutLoginGUI(){
        leftBackground.getChildren().clear();
        rightBackground.getChildren().clear();

        leftBackground.getChildren().addAll(createProfileButton,loginButton);
        rightBackground.getChildren().addAll(headingLabel,menuGrid,closeButton);
    }

    public void layoutPreGameGUI(){
        startButton.setDisable(true);
        gameGrid.gameGrid();
        leftBackground.getChildren().clear();
        leftBackground.getChildren().addAll(profileButton,gameModeSelection, startButton);

        rightBackground.getChildren().clear();
        rightBackground.getChildren().addAll(headingLabel,menuGrid,closeButton);
        wordProgress.setText("");
        word = "";
        gridConnector.reset();
    }

    public void layoutLevelSelectionGUI() {
        gridConnector.hardReset();
        updateLevelSelectionGrid(gameData.getLevelsCompleted());
        gameModeLabel.setText(selection);
        gameModeLabel.layoutYProperty().bind(rightBackground.heightProperty().divide(5.9));
        leftBackground.getChildren().removeAll(gameModeSelection, startButton);
        leftBackground.getChildren().addAll(homeButton);

        rightBackground.getChildren().remove(menuGrid);
        rightBackground.getChildren().addAll(gameModeLabel,levelSelectionGrid);
    }

    public void layoutGameGUI(int level) {
        if (level >= 8)
            nextButton.setDisable(true);
        wordProgress.setText("");
        gameGrid.createGame();
        gameGrid.printWords();
        totalScore = 0;
        sampleData.clear();
        GameData gameData = (GameData) app.getDataComponent();
        this.level = level;
        //Makes sure the level is playable
        if (level <= gameData.getLevelsCompleted() + 1) {
            gameModeLabel.layoutYProperty().bind(rightBackground.heightProperty().divide(50));
            rightBackground.getChildren().clear();
            rightBackground.getChildren().addAll(gameModeLabel,gameGridPane, timeRemainingLabel, wordProgress, pointTable, target, levelLabel, playPauseButton, replayButton, nextButton);
        }
        levelLabel.setText("Level " + level);
        playPauseButton.setText("Pause");
        gameGridPane.setVisible(true);

        PropertyManager propertyManager = PropertyManager.getManager();
        app.getWorkspaceComponent().getWorkspace().setOnKeyTyped(e -> {
            if (Character.isLetter(e.getCharacter().charAt(0))) {
                char input = e.getCharacter().charAt(0);
                for (Node node : gameGrid.getChildren()) {
                    LetterCircle circle = (LetterCircle) node;
                    int columnTemp = GridPane.getColumnIndex(circle);
                    int rowTemp = GridPane.getRowIndex(circle);
                    if (gameGrid.usedCircles.isEmpty())
                        if (circle.getText().getText().equalsIgnoreCase(String.valueOf(input))) {
                            circle.getStyleClass().add(propertyManager.getPropertyValue(CIRCLE_SELECTED));
                            circle.setUsed(true);
                            gameGrid.addUsedCircle(circle);
                            gridConnector.wordConnector(gameGrid.usedCircles,gameGrid);
                        }
                    if (gameGrid.adjacentCircles(columnTemp,rowTemp)) {
                        if (circle.getText().getText().equalsIgnoreCase(String.valueOf(input))) {
                            circle.getStyleClass().add(propertyManager.getPropertyValue(CIRCLE_SELECTED));
                            circle.setUsed(true);
                            gameGrid.addUsedCircle(circle);
                            gridConnector.wordConnector(gameGrid.usedCircles,gameGrid);
                        }
                    }
                    else {
                        if (circle.getText().getText().equalsIgnoreCase(String.valueOf(input))) {
                            circle.getStyleClass().add(propertyManager.getPropertyValue(CIRCLE_SELECTED));
                            circle.setUsed(true);
                            gameGrid.addUsedCircle(circle);
                            gridConnector.wordConnector(gameGrid.usedCircles,gameGrid);
                        }
                    }
                }
            }
        });

        app.getWorkspaceComponent().getWorkspace().setOnKeyPressed(e ->{
            if (e.getCode() == KeyCode.ENTER) {
                addWord();
                gridConnector.hardReset();
                gameGrid.resetGrid();
            }
        });

        startTimer();
    }

    public void layoutGameGUI(int column, int row) {
        if (level >= 8)
            nextButton.setDisable(true);
        gameGrid.createGame();
        gameGrid.printWords();
        totalScore = 0;
        sampleData.clear();
        GameData gameData = (GameData) app.getDataComponent();
        level = Integer.parseInt((levelSelectionGrid.getText(column,row)));
        //Makes sure the level is playable
        if (level <= gameData.getLevelsCompleted() + 1) {
            gameModeLabel.layoutYProperty().bind(rightBackground.heightProperty().divide(50));
            rightBackground.getChildren().clear();
            rightBackground.getChildren().addAll(gameModeLabel, closeButton, gameGridPane, timeRemainingLabel, wordProgress, pointTable, target, levelLabel, playPauseButton, replayButton, nextButton);
        }
        levelLabel.setText("Level " + level);
        playPauseButton.setText("Pause");
        gameGridPane.setVisible(true);

        PropertyManager propertyManager = PropertyManager.getManager();
        app.getWorkspaceComponent().getWorkspace().setOnKeyTyped(e -> {
            if (Character.isLetter(e.getCharacter().charAt(0))) {
                char input = e.getCharacter().charAt(0);
                for (Node node : gameGrid.getChildren()) {
                    LetterCircle circle = (LetterCircle) node;
                    int columnTemp = GridPane.getColumnIndex(circle);
                    int rowTemp = GridPane.getRowIndex(circle);
                    if (gameGrid.usedCircles.isEmpty())
                        if (circle.getText().getText().equalsIgnoreCase(String.valueOf(input))) {
                            circle.getStyleClass().add(propertyManager.getPropertyValue(CIRCLE_SELECTED));
                            circle.setUsed(true);
                            gameGrid.addUsedCircle(circle);
                            gridConnector.wordConnector(gameGrid.usedCircles,gameGrid);
                        }
                    if (gameGrid.adjacentCircles(columnTemp,rowTemp)) {
                        if (circle.getText().getText().equalsIgnoreCase(String.valueOf(input))) {
                            circle.getStyleClass().add(propertyManager.getPropertyValue(CIRCLE_SELECTED));
                            circle.setUsed(true);
                            gameGrid.addUsedCircle(circle);
                            gridConnector.wordConnector(gameGrid.usedCircles,gameGrid);
                        }
                    }
                    else {
                        if (circle.getText().getText().equalsIgnoreCase(String.valueOf(input))) {
                            circle.getStyleClass().add(propertyManager.getPropertyValue(CIRCLE_SELECTED));
                            circle.setUsed(true);
                            gameGrid.addUsedCircle(circle);
                            gridConnector.wordConnector(gameGrid.usedCircles,gameGrid);
                        }
                    }
                }
            }
        });

        app.getWorkspaceComponent().getWorkspace().setOnKeyPressed(e ->{
            if (e.getCode() == KeyCode.ENTER) {
                addWord();
                gridConnector.hardReset();
                gameGrid.resetGrid();
            }
        });

        startTimer();
    }

    public int setDifficulty() {
        switch (level) {
            case 1: return 120;
            case 2: return 110;
            case 3: return 100;
            case 4: return 90;
            case 5: return 80;
            case 6: return 70;
            case 7: return 65;
            case 8: return 60;
        }
        return 120;
    }

    public void startTimer(){
        if (timeline != null)
            timeline.stop();
        timeLeft = setDifficulty();
        timeRemainingLabel.setText("Time Remaining:\n" + timeLeft + " Seconds");

        timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(1), event -> {
            timeLeft--;
            timeRemainingLabel.setText("Time Remaining:\n" + timeLeft + " Seconds");
            if (timeLeft <= 0) {
                timeline.stop();
                gameGrid.removeHandlers();
                app.getWorkspaceComponent().getWorkspace().setOnKeyTyped(e -> {
                });
                app.getWorkspaceComponent().getWorkspace().setOnKeyPressed(e ->{
                    if (e.getCode() == KeyCode.ENTER) {
                        AppMessageDialogSingleton messageDialogSingleton = AppMessageDialogSingleton.getSingleton();
                        messageDialogSingleton.show("Time is up!", "Time is up.\nThis word will not be added to your final score.");
                    }
                });
                gameGrid.showSolution();
            }
        }));
        timeline.playFromStart();
    }

    public void addWord() {
        targetScore = gameGrid.getTargetScore();
        String word = "";
        int score = 0;
        for (LetterCircle circle : gameGrid.usedCircles) {
            if (!word.contains(circle.getText().getText())) {
                word = word.concat(circle.getText().getText());
            }
        }
        wordProgress.setText(word);
        for (char[] usedWord : gameGrid.usedWords) {
            String dictWord = String.valueOf(usedWord);
            if (dictWord.equalsIgnoreCase(word)) {
                for (int i = 0; i < usedWord.length; i++) {
                    score += 5;
                    totalScore += 5;
                }
                sampleData.add(new wordPoint(word, score));
            }
            totalPoints.setText("Total Score:\n" + totalScore + " Points");
        }
        if (totalScore == targetScore && totalScore > 0) {
            AppMessageDialogSingleton messageDialogSingleton = AppMessageDialogSingleton.getSingleton();
            messageDialogSingleton.show("You win!", "You finished the level!");
            if (level == gameData.getLevelsCompleted() + 1)
                gameData.addLevelCompleted();
            nextButton.setDisable(false);
            timeline.stop();
        }
    }

    public void end() {
        gameGridPane.setVisible(false);
    }

    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();

        leftBackground.getStyleClass().add(propertyManager.getPropertyValue(LEFT_PANE));
        rightBackground.getStyleClass().add(propertyManager.getPropertyValue(RIGHT_PANE));
        createProfileButton.getStyleClass().add(propertyManager.getPropertyValue(BUTTON));
        loginButton.getStyleClass().add(propertyManager.getPropertyValue(BUTTON));
        headingLabel.getStyleClass().add(propertyManager.getPropertyValue(HEADING_LABEL));
        menuGrid.setCircleStyle(propertyManager.getPropertyValue(CIRCLE));
        profileButton.getStyleClass().add(propertyManager.getPropertyValue(BUTTON));
        startButton.getStyleClass().add(propertyManager.getPropertyValue(BUTTON));
        gameModeLabel.getStyleClass().add(propertyManager.getPropertyValue(GAME_MODE_LABEL));
        timeRemainingLabel.getStyleClass().add(propertyManager.getPropertyValue(GAME_LABEL));
        wordProgress.getStyleClass().add(propertyManager.getPropertyValue(GAME_LABEL));
        gameProgress.getStyleClass().add(propertyManager.getPropertyValue(TABLE));
        target.getStyleClass().add(propertyManager.getPropertyValue(GAME_LABEL));
        levelLabel.getStyleClass().add(propertyManager.getPropertyValue(GAME_MODE_LABEL));
        playPauseButton.getStyleClass().add(propertyManager.getPropertyValue(BUTTON));
        totalPoints.getStyleClass().add(propertyManager.getPropertyValue(GAME_LABEL));
        replayButton.getStyleClass().add(propertyManager.getPropertyValue(BUTTON));
        nextButton.getStyleClass().add(propertyManager.getPropertyValue(BUTTON));
    }

    @Override
    public void reloadWorkspace() {

    }

    public void updateLevelSelectionGrid(int levelsCompleted) {
        levelSelectionGrid.updateLevelSelectionGrid(levelsCompleted);
        for (Node c : levelSelectionGrid.getChildren()) {
            LetterCircle circle = (LetterCircle) c;
            int column = GridPane.getColumnIndex(c);
            int row = GridPane.getRowIndex(c);
            c.setOnMouseClicked(event -> {
                layoutGameGUI(column, row);
                target.setText("Target: \n" + String.valueOf(gameGrid.getTargetScore()) + " Points");
                selectedColumn = column;
                selectedRow = row;
            });
        }
    }

    public void addLetter(String letter, int column, int row) {
        word = word.concat(letter);
        wordProgress.setText(word);
        gridConnector.highlightConnector(column,row);
    }

    public void removeLetter(String text) {
        word = "";
        wordProgress.setText(word);
    }

    public class wordPoint {
        private final SimpleStringProperty word;
        private final SimpleIntegerProperty points;

        private wordPoint(String word, int points) {
            this.word = new SimpleStringProperty(word);
            this.points = new SimpleIntegerProperty(points);
        }

        public String getWord(){
            return word.get();
        }

        public int getPoints() {
            return points.get();
        }
    }

    public void logout(){
        layoutLoginGUI();

        GameData gameData = (GameData) app.getDataComponent();
        gameData.logout();
    }

    public GridConnector getGridConnector() {
        return gridConnector;
    }
}
