package gui;

import apptemplate.AppTemplate;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;

/**
 * Created by David on 12/3/2016.
 */
public class GridConnector extends StackPane {

    AppTemplate appTemplate;

    Insets horizontalMargin;
    Insets verticalMargin;

    GridPane horizontalGridPane;
    GridPane verticalGridPane;

    HashSet<Line> usedLines = new HashSet<>();

    public GridConnector(AppTemplate appTemplate) {
        super();
        horizontalGridPane = new GridPane();
        verticalGridPane = new GridPane();
        this.getChildren().addAll(horizontalGridPane,verticalGridPane);
        this.appTemplate = appTemplate;
        createHorizontalConnectors(3, 4);
        createVerticalConnectors(4,3);
    }

    public void createHorizontalConnectors(int column, int row) {
        horizontalMargin = new Insets(65, 30, 60, 43);
        for (int i = 0; i < column; i++) {
            for (int j = 0; j < row; j++) {
                Line line = new Line();
                line.setStartX(0);
                line.setEndX(50);
                GridPane.setConstraints(line, i, j);
                GridPane.setMargin(line, horizontalMargin);
                line.setStroke(Paint.valueOf("#223768"));
                line.setStrokeWidth(5);
                horizontalGridPane.getChildren().add(line);
            }
        }
    }

    private void createVerticalConnectors(int column, int row) {
        verticalMargin = new Insets(80, 122, 0, 3);
        for (int i = 0; i < column; i++) {
            for (int j = 0; j < row; j++) {
                Line line = new Line();
                line.setStartY(0);
                line.setEndY(60);
                GridPane.setConstraints(line, i, j);
                GridPane.setMargin(line, verticalMargin);
                line.setStroke(Paint.valueOf("#223768"));
                line.setStrokeWidth(5);
                verticalGridPane.getChildren().add(line);
            }
        }
    }

    public void highlightConnector(int column, int row) {
        reset();
        for (Node node : horizontalGridPane.getChildren()) {
            if (GridPane.getColumnIndex(node) == column && GridPane.getRowIndex(node) == row) {
                Line line = (Line) node;
                line.setStroke(Paint.valueOf("#FF4C4C"));
            }
            if (GridPane.getColumnIndex(node) == column - 1 && GridPane.getRowIndex(node) == row) {
                Line line = (Line) node;
                line.setStroke(Paint.valueOf("#FF4C4C"));
            }
        }
        for (Node node : verticalGridPane.getChildren()) {
            if (GridPane.getColumnIndex(node) == column && GridPane.getRowIndex(node) == row) {
                Line line = (Line) node;
                line.setStroke(Paint.valueOf("#FF4C4C"));
            }
            if (GridPane.getColumnIndex(node) == column && GridPane.getRowIndex(node) == row - 1) {
                Line line = (Line) node;
                line.setStroke(Paint.valueOf("#FF4C4C"));
            }
        }
    }

    public void wordConnector(LinkedHashSet<LetterCircle> usedCircles, LetterCircleGrid gameGrid) {
        ArrayList<LetterCircle> letterCircles = new ArrayList<>(usedCircles);
        for (int i = 0; i < letterCircles.size() - 1; i++) {
            for (int j = 0; j < letterCircles.size() - 1; j++) {
                LetterCircle current = letterCircles.get(i);
                LetterCircle next = letterCircles.get(j);
                if (GridPane.getColumnIndex(current) - 1 == GridPane.getColumnIndex(next) && usedCircles.contains(gameGrid.getCircle(GridPane.getColumnIndex(current) - 1, GridPane.getRowIndex(current)))) {
                    for (Node node : horizontalGridPane.getChildren()) {
                        if (GridPane.getColumnIndex(node) == GridPane.getColumnIndex(current) - 1 && GridPane.getRowIndex(node) == GridPane.getRowIndex(current)) {
                            Line line = (Line) node;
                            line.setStroke(Paint.valueOf("#FF4C4C"));
                            usedLines.add(line);
                        }
                    }
                } else if (GridPane.getColumnIndex(current) + 1 == GridPane.getColumnIndex(next) && usedCircles.contains(gameGrid.getCircle(GridPane.getColumnIndex(current) + 1, GridPane.getRowIndex(current)))) {
                    for (Node node : horizontalGridPane.getChildren()) {
                        if (GridPane.getColumnIndex(node) == GridPane.getColumnIndex(current) && GridPane.getRowIndex(node) == GridPane.getRowIndex(current)) {
                            Line line = (Line) node;
                            line.setStroke(Paint.valueOf("#FF4C4C"));
                            usedLines.add(line);
                        }
                    }
                } else if (GridPane.getRowIndex(current) - 1 == GridPane.getRowIndex(next) && usedCircles.contains(gameGrid.getCircle(GridPane.getColumnIndex(current), GridPane.getRowIndex(current) - 1))) {
                    for (Node node : verticalGridPane.getChildren()) {
                        if (GridPane.getColumnIndex(node) == GridPane.getColumnIndex(current) && GridPane.getRowIndex(node) == GridPane.getRowIndex(current) - 1) {
                            Line line = (Line) node;
                            line.setStroke(Paint.valueOf("#FF4C4C"));
                            usedLines.add(line);
                        }
                    }
                } else if (GridPane.getRowIndex(current) + 1 == GridPane.getRowIndex(next) && usedCircles.contains(gameGrid.getCircle(GridPane.getColumnIndex(current), GridPane.getRowIndex(current) + 1))) {
                    for (Node node : verticalGridPane.getChildren()) {
                        if (GridPane.getColumnIndex(node) == GridPane.getColumnIndex(current) && GridPane.getRowIndex(node) == GridPane.getRowIndex(current)) {
                            Line line = (Line) node;
                            line.setStroke(Paint.valueOf("#FF4C4C"));
                            usedLines.add(line);
                        }
                    }
                }
            }
        }
    }

    public void reset() {
        for (Node node : horizontalGridPane.getChildren()) {
            Line line = (Line) node;
            if (!usedLines.contains(line))
                line.setStroke(Paint.valueOf("#223768"));
        }
        for (Node node : verticalGridPane.getChildren()) {
            Line line = (Line) node;
            if (!usedLines.contains(line))
                line.setStroke(Paint.valueOf("#223768"));
        }
    }

    public void hardReset() {
        for (Node node : horizontalGridPane.getChildren()) {
            Line line = (Line) node;
            line.setStroke(Paint.valueOf("#223768"));
        }
        for (Node node : verticalGridPane.getChildren()) {
            Line line = (Line) node;
            line.setStroke(Paint.valueOf("#223768"));
        }
    }
}
