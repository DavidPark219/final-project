package gui;

import apptemplate.AppTemplate;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import propertymanager.PropertyManager;

import java.io.*;
import java.util.*;

import static buzzWord.BuzzWordProperties.CIRCLE;
import static buzzWord.BuzzWordProperties.CIRCLE_SELECTED;

/**
 * Created by David on 11/11/2016.
 */
public class LetterCircleGrid extends GridPane {

    final File dictionaryResource = new File("Buzzword/resources/dictionary/dictionary.txt");
    final File placesResource = new File("Buzzword/resources/dictionary/places.txt");
    final File foodResource = new File("Buzzword/resources/dictionary/food.txt");
    Insets margin;

    HashSet<String> dictionary;
    HashSet<String> places;
    HashSet<String> food;
    HashSet<char[]> usedWords;

    String selection;

    AppTemplate appTemplate;
    private char[] startingWord;
    private int startingColumn;
    private int startingRow;

    LinkedHashSet<LetterCircle> usedCircles;
    LinkedHashSet<LetterCircle> solution;

    public LetterCircleGrid(AppTemplate appTemplate) {
        super();
        margin = new Insets(15, 15, 15, 15);
        this.appTemplate = appTemplate;
    }

    public void createGrid(int column, int row) {
        for (int i = 0; i < column; i++) {
            for (int j = 0; j < row; j++) {
                LetterCircle letterCircle = new LetterCircle();
                GridPane.setConstraints(letterCircle, i, j);
                GridPane.setMargin(letterCircle, margin);
                this.getChildren().add(letterCircle);
            }
        }
    }

    public void setCircleStyle(String style) {
        for (Node c : this.getChildren()) {
            c.getStyleClass().add(style);
        }
    }

    public void buzzWordGrid() {
        createGrid(4, 4);
        LetterCircle circle = (LetterCircle) getCircle(0, 0);
        circle.addLetter("B");
        LetterCircle circle2 = (LetterCircle) getCircle(1, 0);
        circle2.addLetter("U");
        LetterCircle circle3 = (LetterCircle) getCircle(0, 1);
        circle3.addLetter("Z");
        LetterCircle circle4 = (LetterCircle) getCircle(1, 1);
        circle4.addLetter("Z");
        LetterCircle circle5 = (LetterCircle) getCircle(2, 2);
        circle5.addLetter("W");
        LetterCircle circle6 = (LetterCircle) getCircle(3, 2);
        circle6.addLetter("O");
        LetterCircle circle7 = (LetterCircle) getCircle(2, 3);
        circle7.addLetter("R");
        LetterCircle circle8 = (LetterCircle) getCircle(3, 3);
        circle8.addLetter("D");
    }

    public void levelSelectionGrid(int levelsCompleted) {
        PropertyManager propertyManager = PropertyManager.getManager();
        createGrid(4, 2);
        if (levelsCompleted < 4) {
            for (int i = 0; i < levelsCompleted + 1; i++) {
                LetterCircle circle = (LetterCircle) getCircle(i, 0);
                circle.addLetter(Integer.toString(i + 1));
                circle.getCircle().setFill(Paint.valueOf("#FFFFFF"));
                circle.getText().setFill(Paint.valueOf("#000000"));
                circle.getStyleClass().add(propertyManager.getPropertyValue(CIRCLE));
            }
            for (int i = 3; i > levelsCompleted; i--) {
                LetterCircle circle = (LetterCircle) getCircle(i, 0);
                circle.addLetter(Integer.toString(i + 1));
                circle.getStyleClass().add(propertyManager.getPropertyValue(CIRCLE));
            }
            for (int i = 0; i < 4; i++) {
                LetterCircle circle = (LetterCircle) getCircle(i, 1);
                circle.addLetter(Integer.toString(i + 5));
                circle.getStyleClass().add(propertyManager.getPropertyValue(CIRCLE));
            }
        }
        else {
            for (int i = 0; i < 4; i++) {
                LetterCircle circle = (LetterCircle) getCircle(i, 0);
                circle.addLetter(Integer.toString(i + 1));
                circle.getCircle().setFill(Paint.valueOf("#FFFFFF"));
                circle.getText().setFill(Paint.valueOf("#000000"));
                circle.getStyleClass().add(propertyManager.getPropertyValue(CIRCLE));
            }
            for (int i = 0; i < levelsCompleted - 3; i++) {
                LetterCircle circle = (LetterCircle) getCircle(i, 1);
                circle.addLetter(Integer.toString(i + 5));
                circle.getCircle().setFill(Paint.valueOf("#FFFFFF"));
                circle.getText().setFill(Paint.valueOf("#000000"));
                circle.getStyleClass().add(propertyManager.getPropertyValue(CIRCLE));
            }
            for (int i = 3; i > levelsCompleted - 4; i--) {
                LetterCircle circle = (LetterCircle) getCircle(i, 1);
                circle.addLetter(Integer.toString(i + 5));
                circle.getStyleClass().add(propertyManager.getPropertyValue(CIRCLE));
            }
        }
    }

    public Node getCircle(int column, int row) {
        for (Node node : this.getChildren()) {
            if (GridPane.getColumnIndex(node) == column && GridPane.getRowIndex(node) == row) {
                return node;
            }
        }
        return null;
    }

    public String getText(int column, int row) {
        for (Node node : this.getChildren()) {
            if (GridPane.getColumnIndex(node) == column && GridPane.getRowIndex(node) == row) {
                LetterCircle circle = (LetterCircle) node;
                return circle.getText().getText();
            }
        }
        return null;
    }

    public Boolean visited(int column, int row) {
        for (Node node : this.getChildren()) {
            if (GridPane.getColumnIndex(node) == column && GridPane.getRowIndex(node) == row) {
                LetterCircle circle = (LetterCircle)node;
                return circle.visited;
            }
        }
        return false;
    }

    public boolean adjacentCircles(int column, int row) {
        for (LetterCircle circle : usedCircles) {
            if (Math.abs(GridPane.getColumnIndex(circle) - column) <= 1 && Math.abs(GridPane.getRowIndex(circle) - row) <= 1) {
                return true;
            }
        }
        return false;
    }

    public void resetVisited(){
        for (Node node : this.getChildren()) {
            LetterCircle circle = (LetterCircle)node;
            circle.setVisited(false);
        }
    }


    public void gameGrid() {
        PropertyManager propertyManager = PropertyManager.getManager();
        selection = "Food";
        dictionary = new HashSet<>();
        places = new HashSet<>();
        food = new HashSet<>();
        usedWords = new HashSet<>();
        initDictionary(setResource());

        usedCircles = new LinkedHashSet<>();
        solution = new LinkedHashSet<>();
        constructGameGrid();
    }

    public void constructGameGrid(){
        this.getChildren().clear();
        createGrid(4,4);
    }

    public void printWords() {
        for (char[] chars : usedWords) {
            System.out.println(chars);
        }
    }

    public void createGame() {
        usedWords.clear();
        usedCircles.clear();
        resetVisited();

        Random r = new Random();
        startingColumn = r.nextInt(4);
        startingRow = r.nextInt(4);
        startingWord = randomString().toCharArray();
        usedWords.add(startingWord);
        int counter = 0;
        constructGameGrid();
        generateWords(startingColumn,startingRow,counter,startingWord);
        int wordsCounter = 1;
        while (wordsCounter < 2) {
            addWords();
            wordsCounter++;
        }
        generateLetters();
    }

    public void resetGrid() {
        usedCircles.clear();
        PropertyManager propertyManager = PropertyManager.getManager();
        for (Node node : this.getChildren()) {
            LetterCircle circle = (LetterCircle) node;
            circle.getStyleClass().clear();
            circle.getStyleClass().add(propertyManager.getPropertyValue(CIRCLE));
            circle.setUsed(false);
        }
    }

    public boolean generateWords(int column, int row, int counter, char[] word) {
        if (counter < word.length) {
            PropertyManager propertyManager = PropertyManager.getManager();
            LetterCircle circle = (LetterCircle) getCircle(column, row);
            circle.getStyleClass().add(propertyManager.getPropertyValue(CIRCLE));
            circle.getCircle().setFill(Paint.valueOf("#354981"));
            circle.addLetter(String.valueOf(word[counter]).toUpperCase());
            circle.getText().setFont(Font.font("Helvetica", 30));
            circle.setOnMouseClicked(event -> {
                if (!circle.used) {
                    if (adjacentCircles(GridPane.getColumnIndex(circle),GridPane.getRowIndex(circle)) || usedCircles.isEmpty()) {
                        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
                        circle.getStyleClass().add(propertyManager.getPropertyValue(CIRCLE_SELECTED));
                        circle.setUsed(true);
                        usedCircles.add(circle);
                        GridConnector gridConnector = workspace.getGridConnector();
                        gridConnector.wordConnector(usedCircles, this);
                    }
                }
            });
            solution.add(circle);
            circle.setVisited(true);
            counter++;
            Random r = new Random();
            long startTime = System.currentTimeMillis();
            while(System.currentTimeMillis() - startTime < 10) {
                int random = r.nextInt(4);
                if (random == 1 || random == 3) {
                    if (column + 1 < 4 && !visited(column + 1, row)) {
                        generateWords(column + 1, row, counter, word);
                        return true;
                    }
                }
                if (random == 2 || random == 4) {
                    if (row + 1 < 4 && !visited(column, row + 1)) {
                        generateWords(column, row + 1, counter, word);
                        return true;
                    }
                }
                if (random == 1 || random == 3) {
                    if (column - 1 >= 0 && !visited(column - 1, row)) {
                        generateWords(column - 1, row, counter, word);
                        return true;
                    }
                }
                if (random == 2 || random == 4) {
                    if (row - 1 >= 0 && !visited(column, row - 1)) {
                        generateWords(column, row - 1, counter, word);
                        return true;
                    }
                }
            }
            createGame();
            return false;
        }
        return false;
    }

    public void addWords() {
        Random r = new Random();
        int column = r.nextInt(4);
        int row = r.nextInt(4);
        char[] word = randomString().toCharArray();
        for (char[] chars : usedWords) {
            if (String.valueOf(chars).equalsIgnoreCase(String.valueOf(word)))
                word = randomString().toCharArray();
        }
        usedWords.add(word);
        while (visited(column, row)) {
            column = r.nextInt(4);
            row = r.nextInt(4);
        }
        int counter = 0;
        generateWords(column, row, counter, word);
    }

    public void generateLetters() {
        for (Node node : this.getChildren()) {
            LetterCircle circle = (LetterCircle) node;
            if (!circle.visited) {
                Random r = new Random();
                PropertyManager propertyManager = PropertyManager.getManager();
                circle.getStyleClass().add(propertyManager.getPropertyValue(CIRCLE));
                circle.getCircle().setFill(Paint.valueOf("#354981"));
                circle.addLetter(Character.toString((char)(r.nextInt(26) + 'A')));
                circle.getText().setFont(Font.font("Helvetica", 30));
                circle.setOnMouseClicked(event -> {
                    if (!circle.used) {
                        if (adjacentCircles(GridPane.getColumnIndex(circle),GridPane.getRowIndex(circle)) || usedCircles.isEmpty()) {
                            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
                            circle.getStyleClass().add(propertyManager.getPropertyValue(CIRCLE_SELECTED));
                            circle.setUsed(true);
                            usedCircles.add(circle);
                            GridConnector gridConnector = workspace.getGridConnector();
                            gridConnector.wordConnector(usedCircles, this);
                        }
                    }
                });
                circle.setVisited(true);
            }
        }
    }

    public String randomString() {
        int size = setDictionary().size();
        int item = new Random().nextInt(size);
        int i = 0;
        for(String word : setDictionary())
        {
            if (i == item)
                return word;
            i = i + 1;
        }
        return null;
    }

    public int getTargetScore() {
        int total = 0;
        for (char[] word : usedWords) {
            for (int i = 0; i < word.length; i++) {
                total += 5;
            }
        }
        return total;
    }

    public File setResource() {
        switch(selection) {
            case "English Dictionary" :
                return dictionaryResource;
            case "Places" :
                return placesResource;
            case "Food" :
                return foodResource;
        }
        return null;
    }

    public HashSet<String> setDictionary() {
        switch(selection) {
            case "English Dictionary" :
                return dictionary;
            case "Places" :
                return places;
            case "Food" :
                return food;
        }
        return null;
    }

    public void initDictionary(File resource) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(resource))) {
            if (setDictionary().isEmpty()) {
                String word;
                while ((word = bufferedReader.readLine()) != null) {
                    if (!word.contains("'") && !word.contains("\"") && word.length() <= 16 && word.length() > 1) {
                        setDictionary().add(word.trim().toLowerCase());
                    }
                }
            }
        } catch (IOException e) {
        }
    }



    public void updateLevelSelectionGrid(int levelsCompleted) {
        this.getChildren().clear();
        PropertyManager propertyManager = PropertyManager.getManager();
        createGrid(4,2);
        if (levelsCompleted < 4) {
            for (int i = 0; i < levelsCompleted + 1; i++) {
                LetterCircle circle = (LetterCircle) getCircle(i, 0);
                circle.addLetter(Integer.toString(i + 1));
                circle.getCircle().setFill(Paint.valueOf("#FFFFFF"));
                circle.getText().setFill(Paint.valueOf("#000000"));
                circle.getStyleClass().add(propertyManager.getPropertyValue(CIRCLE));
            }
            for (int i = 3; i > levelsCompleted; i--) {
                LetterCircle circle = (LetterCircle) getCircle(i, 0);
                circle.addLetter(Integer.toString(i + 1));
                circle.getStyleClass().add(propertyManager.getPropertyValue(CIRCLE));
            }
            for (int i = 0; i < 4; i++) {
                LetterCircle circle = (LetterCircle) getCircle(i, 1);
                circle.addLetter(Integer.toString(i + 5));
                circle.getStyleClass().add(propertyManager.getPropertyValue(CIRCLE));
            }
        }
        else if (levelsCompleted == 8) {
            for (int i = 0; i < 4; i++) {
                LetterCircle circle = (LetterCircle) getCircle(i, 0);
                circle.addLetter(Integer.toString(i + 1));
                circle.getCircle().setFill(Paint.valueOf("#FFFFFF"));
                circle.getText().setFill(Paint.valueOf("#000000"));
                circle.getStyleClass().add(propertyManager.getPropertyValue(CIRCLE));
            }
            for (int i = 0; i < 4; i++) {
                LetterCircle circle = (LetterCircle) getCircle(i, 1);
                circle.addLetter(Integer.toString(i + 5));
                circle.getCircle().setFill(Paint.valueOf("#FFFFFF"));
                circle.getText().setFill(Paint.valueOf("#000000"));
                circle.getStyleClass().add(propertyManager.getPropertyValue(CIRCLE));
            }
        }
        else {
            for (int i = 0; i < 4; i++) {
                LetterCircle circle = (LetterCircle) getCircle(i, 0);
                circle.addLetter(Integer.toString(i + 1));
                circle.getCircle().setFill(Paint.valueOf("#FFFFFF"));
                circle.getText().setFill(Paint.valueOf("#000000"));
                circle.getStyleClass().add(propertyManager.getPropertyValue(CIRCLE));
            }
            for (int i = 0; i < levelsCompleted - 3; i++) {
                LetterCircle circle = (LetterCircle) getCircle(i, 1);
                circle.addLetter(Integer.toString(i + 5));
                circle.getCircle().setFill(Paint.valueOf("#FFFFFF"));
                circle.getText().setFill(Paint.valueOf("#000000"));
                circle.getStyleClass().add(propertyManager.getPropertyValue(CIRCLE));
            }
            for (int i = 3; i > levelsCompleted - 4; i--) {
                LetterCircle circle = (LetterCircle) getCircle(i, 1);
                circle.addLetter(Integer.toString(i + 5));
                circle.getStyleClass().add(propertyManager.getPropertyValue(CIRCLE));
            }
        }
    }

    public void updateSelection(String selection) {
        this.selection = selection;
        initDictionary(setResource());
    }

    public void addUsedCircle(LetterCircle circle) {
        usedCircles.add(circle);
    }

    public void removeHandlers() {
        for (Node node : this.getChildren()) {
            node.setOnMouseClicked(event -> {
            });
        }
    }

    public void showSolution() {
        PropertyManager propertyManager = PropertyManager.getManager();
        for (LetterCircle circle : solution) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            circle.getStyleClass().clear();
            circle.getStyleClass().add(propertyManager.getPropertyValue(CIRCLE_SELECTED));
            GridConnector gridConnector = workspace.getGridConnector();
            gridConnector.wordConnector(solution, this);
        }
    }
}
